import { Component, OnInit} from '@angular/core';
import { Store } from '@ngrx/store';
import { combineLatest, fromEvent, Observable, of, EMPTY, merge } from 'rxjs';
import { AppState } from 'src/app/store/app.state';
import { SelectDontWantIngredients, selectDontWantIngredientsFeature, SelectWantIngredients, selectWantIngredientsFeature } from 'src/app/store/ingredients.selectors';
import { getSpecificPizzas } from 'src/app/store/pizzas.actions';
import { Ingredient } from 'src/app/models/ingredient';


@Component({
  selector: 'app-navbar',
  templateUrl: './app-navbar.component.html',
  styleUrls: ['./app-navbar.component.css'],
  
})
export class AppNavbarComponent implements OnInit {

  wantIngredients: Observable<Ingredient[]> = of([]);
  dontWantIngredients: Observable<readonly Ingredient[]> = of([]);

  constructor(private store: Store<AppState>) { }

  inputWantElement: Element | null = null;
  inputDontWantElement: Element | null = null;

  inputCheckboxNoMeat: Element | null = null;
  inputCheckboxFasting: Element | null = null;

  isCheckedNoMeat : boolean = false;
  isCheckedFasting : boolean = false;

  wantedIngredients: readonly Ingredient[] = [];
  $wi = this.store.select(SelectWantIngredients).subscribe(data => {
      if(data)
      this.wantedIngredients = data;
      else this.wantedIngredients = [];
    })

    dontWantedIngredients: readonly Ingredient[] = [];
    $dwi = this.store.select(SelectDontWantIngredients).subscribe(data => {
      if(data)
      this.dontWantedIngredients = data;
      else this.dontWantedIngredients = [];
    })

  ngOnInit(): void {

    this.inputWantElement = document.querySelector(".input-want");
    this.inputDontWantElement = document.querySelector(".input-dont-want");

    this.inputCheckboxNoMeat = document.querySelector(".checkboxNoMeat");
    this.inputCheckboxFasting = document.querySelector(".checkboxFasting");

    let $changeValueCheckboxNoMeat;
    if(this.inputCheckboxNoMeat != null)
      $changeValueCheckboxNoMeat = fromEvent(this.inputCheckboxNoMeat, 'change');
    else $changeValueCheckboxNoMeat = EMPTY;

    let $changeValueCheckboxFasting;
    if(this.inputCheckboxFasting != null)
        $changeValueCheckboxFasting = fromEvent(this.inputCheckboxFasting, 'change');
    else $changeValueCheckboxFasting = EMPTY;

    this.wantIngredients = this.store.select(SelectWantIngredients);
    this.dontWantIngredients = this.store.select(SelectDontWantIngredients);

    // let wantedIngredients: readonly Ingredient[];
    // let $wi = this.store.select(SelectWantIngredients).subscribe(data => {
    //   if(data)
    //   wantedIngredients = data;
    //   else wantedIngredients = [];
    // })

    // let dontWantedIngredients: readonly Ingredient[];
    // let $dwi = this.store.select(SelectDontWantIngredients).subscribe(data => {
    //   if(data)
    //   dontWantedIngredients = data;
    //   else dontWantedIngredients = [];
    // })

    

    const AddRemoveEvent$ = merge(this.store.select(selectWantIngredientsFeature), this.store.select(selectDontWantIngredientsFeature))
      
    const $CheckUncheckCheckboxNoMeatFasting = merge($changeValueCheckboxNoMeat,  $changeValueCheckboxFasting);

    const AllChanges = merge($CheckUncheckCheckboxNoMeatFasting, AddRemoveEvent$).subscribe(ev => {
      this.store.dispatch(getSpecificPizzas(
        {
            wantedIngredientsID: this.wantedIngredients.map(ingredient => ingredient.id),
            avoidIngredientsID: this.dontWantedIngredients.map(ingredient => ingredient.id),
            daLiJeMeso: !this.isCheckedNoMeat,
            daLiJePosna: this.isCheckedFasting
        }))
    })

  }

  onChangeCheckbox(){
    this.store.dispatch(getSpecificPizzas(
      {
          wantedIngredientsID: this.wantedIngredients.map(ingredient => ingredient.id),
          avoidIngredientsID: this.dontWantedIngredients.map(ingredient => ingredient.id),
          daLiJeMeso: !this.isCheckedNoMeat,
          daLiJePosna: this.isCheckedFasting
      }))
  }

}
    
    // if(this.inputWantElement != undefined)
    // fromEvent(this.inputWantElement, "input")
    // .pipe(
    //   map((ev) => <InputEvent>ev),
    //   debounceTime(500),
    //   map((ev: InputEvent) => (<HTMLInputElement>ev.target).value),
    //   filter((text) => text.length >= 2),
    //   // switchMap((text) => this.getIngredients(text))
    //   map((text) => this.getIngredients(text))
    // )
    // .subscribe((ingredients) => {
    //   this.arrayOfSearchedWantIngredients = (ingredients);
    //   console.log(this.arrayOfSearchedWantIngredients);
    // });

    // if(this.inputDontWantElement != undefined)
    // fromEvent(this.inputDontWantElement, "input")
    // .pipe(
    //   map((ev) => <InputEvent>ev),
    //   debounceTime(500),
    //   map((ev: InputEvent) => (<HTMLInputElement>ev.target).value),
    //   filter((text) => text.length >= 2),
    //   // switchMap((text) => this.getIngredients(text))
    //   map((text) => this.getIngredients(text))
    // )
    // .subscribe((ingredients) => {
    //   this.arrayOfSearchedDontWantIngredients = (ingredients);
    //   console.log(this.arrayOfSearchedDontWantIngredients);
    // });

  // }

// getIngredients(text: string)  {
//   let comparedIngredients: Array<Ingredient> = [];
//   let AllIngredients: readonly Ingredient[] = [];
//     let $dwi = this.store.select(SelectAllIngredients).subscribe(data => {
//       AllIngredients = data;
//     })
  
//     AllIngredients.forEach(i => {
//       if (i.imeSastojka.search(text) >= 0) 
//         comparedIngredients.push(i);
//     })
 
//   return comparedIngredients;
// }


// }
  




