import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { AppState } from 'src/app/store/app.state';
import { Pizza } from 'src/app/models/pizza';
import { PizzasService } from 'src/app/services/pizzas.service';
import { SelectPizzas } from 'src/app/store/pizzas.selectors';
@Component({
  selector: 'pizza-list',
  templateUrl: './pizza-list.component.html',
  styleUrls: ['./pizza-list.component.css']
})
export class PizzaListComponent implements OnInit {
  // @Input() pizzas: Pizza[] = [];

  pizzas: Observable<readonly Pizza[]> = of([]);   

  constructor(private store: Store<AppState>) { //private pizzaService: PizzasService) { }
  }     

  ngOnInit(): void {
    
    // this.store.dispatch(loadPizzas())
    this.pizzas = this.store.select(SelectPizzas);

  }

}
 