import { Component, Input, OnInit } from '@angular/core';
import { Ingredient } from 'src/app/models/ingredient';
import { Pizza } from 'src/app/models/pizza';
import { faCheckSquare } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'pizza-details',
  templateUrl: './pizza-details.component.html',
  styleUrls: ['./pizza-details.component.css']
})
export class PizzaDetailsComponent implements OnInit {

  @Input() pizza: Pizza | null = null;

  faCheckSquare = faCheckSquare;

  pizzaIngredients: string[] = [];
  pizzaCalories: number = 0;
  posna: boolean = true;
  imaMesa: boolean = false;

  constructor() { 
  }

  ngOnInit(): void {
    if(this.pizza)
    this.pizza.ingredients.forEach(ingredient => {
      this.pizzaIngredients.push(ingredient.imeSastojka)
      if(ingredient.daLiJeMeso == true)
        this.imaMesa = true;
      if(ingredient.daLiJePosno != true)
        this.posna = false;
      this.pizzaCalories += ingredient.kalorije;
    }
      );

    
  }

}
