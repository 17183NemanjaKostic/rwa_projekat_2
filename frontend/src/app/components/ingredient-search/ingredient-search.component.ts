import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable, of, ReplaySubject, Subject } from 'rxjs';
import { debounceTime, delay, tap, filter, map, takeUntil } from 'rxjs/operators';
import { AppState } from 'src/app/store/app.state';
import { SelectAllIngredients, selectWantIngredientsFeature } from 'src/app/store/ingredients.selectors';
import { Ingredient } from 'src/app/models/ingredient';
import { putDontWantIngredient, putWantIngredient } from 'src/app/store/ingredients.actions';




@Component({
  selector: 'ingredient-search',
  templateUrl: './ingredient-search.component.html',
  styleUrls: ['./ingredient-search.component.css']
})


export class IngredientSearchComponent implements OnInit, OnDestroy {

  $ingredients: Observable<readonly Ingredient[]> = of([]);
  
  ingredients: readonly Ingredient[] = [];

  constructor(private store: Store<AppState>) { }

  @Input() group: number = 0   // 1 - want ingredient, 2 - dont want ingredient
  
  /** list of ingredients */
  //protected ingredients = this.store.select(SelectAllIngredients);

  /** control for the selected ingredient for server side filtering */
  public ingredientServerSideCtrl: FormControl = new FormControl();

  /** control for filter for server side. */
  public ingredientServerSideFilteringCtrl: FormControl = new FormControl();

  /** indicate search operation is in progress */
  public searching = false;

  /** list of ingredients filtered after simulating server side search */
  public  filteredServerSideIngredients: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);

  /** Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject<void>();

  ngOnInit() {

    
    this.$ingredients = this.store.select(SelectAllIngredients);


    let ingr = this.$ingredients.subscribe(data => {
      this.ingredients = data;
    })
    // listen for search field value changes
    this.ingredientServerSideFilteringCtrl.valueChanges
      .pipe(
        filter(search => !!search),
        tap(() => this.searching = true),
        takeUntil(this._onDestroy),
        debounceTime(200),
        map(search => {
          if (!this.ingredients) {
            return [];
          }

          // simulate server fetching and filtering data
          return this.ingredients.filter(ingredients => ingredients.imeSastojka.toLowerCase().indexOf(search) > -1).map(ingredient => ingredient.imeSastojka);
        }),
        delay(500),
        takeUntil(this._onDestroy)
      )
      .subscribe(filteredIngredients => {
        this.searching = false;
        this.filteredServerSideIngredients.next(filteredIngredients);
      },
        error => {
          // no errors in our simulated example
          this.searching = false;
          // handle error...
        });

  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  add(ingredient: string){
    console.log(ingredient)
    let newIngredient: Ingredient | null = null;
    let ingredients: readonly Ingredient[] = [];

    this.$ingredients.subscribe(data => {
      ingredients = data; 
    });
    
    ingredients.forEach(i => {
      if(ingredient === i.imeSastojka)
      newIngredient = i;
    })
    
    if(newIngredient != null){
      if(this.group === 1)
      this.store.dispatch(putWantIngredient({ingredient: newIngredient}));
      if(this.group === 2)
      this.store.dispatch(putDontWantIngredient({ingredient: newIngredient}));
    }
  }

}
  
