import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IngredientPillButtonComponent } from './ingredient-pill-button.component';

describe('IngredientPillButtonComponent', () => {
  let component: IngredientPillButtonComponent;
  let fixture: ComponentFixture<IngredientPillButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IngredientPillButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IngredientPillButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
