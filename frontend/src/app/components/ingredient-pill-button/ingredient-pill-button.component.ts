import { Component, Input, OnInit } from '@angular/core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.state';
import { removeDontWantIngredient, removeWantIngredient } from 'src/app/store/ingredients.actions';
import { Ingredient } from 'src/app/models/ingredient';

@Component({
  selector: 'app-ingredient-pill-button',
  templateUrl: './ingredient-pill-button.component.html',
  styleUrls: ['./ingredient-pill-button.component.css']
})
export class IngredientPillButtonComponent implements OnInit {

  constructor(private store: Store<AppState>) { }

  faTimes = faTimes;

  @Input() ingredient: Ingredient | null = null;
  @Input() group: number = 0   // 1 - want ingredient, 2 - dont want ingredient

  ngOnInit(): void {
    
  }

  remove() {
    if(this.group == 1) {
      if(this.ingredient)
      this.store.dispatch(removeWantIngredient({ingredient: this.ingredient}))
    }
    else if(this.group == 2) {
      if(this.ingredient)
      this.store.dispatch(removeDontWantIngredient({ingredient: this.ingredient}))
    }
  }

}
