import { createSelector } from "@ngrx/store";
import { AppState } from "./app.state";
import { dontWantIngredientsState, wantIngredientsState } from "./ingredients.reducer";
import { Ingredient } from "../models/ingredient";


// export const selectWantIngredientsFeature = createSelector(
//     (state: AppState) => state.WantIngredients,
//     (WantIngredients) => WantIngredients.ingredients
// );

// export const selectDontWantIngredientsFeature = createSelector(
//     (state: AppState) => state.DontWantIngredients,
//     (DontWantIngredients) => DontWantIngredients.ingredients
// );

export const selectAllIngredientsFeature = (state: AppState) => state.AllIngredients;

export const SelectAllIngredients = createSelector(
    selectAllIngredientsFeature,
    (state) => Object
        .values(state.entities)
        .filter(ingredient => ingredient != null)
        .map(ingredient => <Ingredient>ingredient)
)

export const selectWantIngredientsFeature = (state: AppState) => state.WantIngredients;

export const SelectWantIngredients = createSelector(
    selectWantIngredientsFeature,
    (state) => Object
        .values(state.entities)
        .filter(ingredient => ingredient != null)
        .map(ingredient => <Ingredient>ingredient)
)

export const selectDontWantIngredientsFeature = (state: AppState) => state.DontWantIngredients;

export const SelectDontWantIngredients = createSelector(
    selectDontWantIngredientsFeature,
    (state) => Object
        .values(state.entities)
        .filter(ingredient => ingredient != null)
        .map(ingredient => <Ingredient>ingredient)
)


// export const selectWantIngredientsFeature = (state: AppState) => state.WantIngredients;

// export const selectDontWantIngredientsFeature = (state: AppState) => state.DontWantIngredients;

// export const SelectWantIngredients = createSelector(
//     selectWantIngredientsFeature,
//     (state: wantIngredientsState) => Object
//         .values(state.entities)
//         .filter(ingredient => ingredient != null)
//         .map(ingredient => <Ingredients>ingredient) 
// )

// export const SelectDontWantIngredients = createSelector(
//     selectDontWantIngredientsFeature,
//     (state: dontWantIngredientsState) => Object
//         .values(state.entities)
//         .filter(ingredient => ingredient != null)
//         .map(ingredient => <Ingredients>ingredient) 
// )

