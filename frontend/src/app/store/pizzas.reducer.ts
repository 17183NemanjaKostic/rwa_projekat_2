import { state } from "@angular/animations";
import { createEntityAdapter, EntityState } from "@ngrx/entity";
import { createReducer, on } from "@ngrx/store";
import { Pizza } from "../models/pizza";
import * as Actions from "../store/pizzas.actions"

export interface PizzaState extends EntityState<Pizza> {
    //SortedPizzas: ReadonlyArray<Pizza>;  // Pice sa specificnim sastojcima
}

const adapter = createEntityAdapter<Pizza>()

export const initialState: PizzaState = adapter.getInitialState();
    


export const PizzaReducer = createReducer(
    initialState,
    on(Actions.loadPizzasSuccess, (state, {pizzas}) => adapter.setAll(pizzas, state)
    )
    );
