import { state } from "@angular/animations";
import { createSelector } from "@ngrx/store";
import { AppState } from "./app.state";
import { Pizza } from "../models/pizza";

export const selectPizzasFeature = (state: AppState) => state.pizzas;

export const SelectPizzas = createSelector(
    selectPizzasFeature,
    (state) => Object
        .values(state.entities)
        .filter(pizza => pizza != null)
        .map(pizza => <Pizza>pizza)
)