import { createAction, props } from "@ngrx/store";
import { Ingredient } from "../models/ingredient";
import { Pizza } from "../models/pizza";


export const loadAllIngredientsSucces = createAction(
    "Load All Ingredients Success",
    props<{ingredients: Ingredient[]}>()
);

export const loadAllIngredients = createAction(
    "Load All Ingredients"
);

export const getWantIngredients = createAction(
    "Get Want ingredients",
    props<{
        ingredients: Ingredient[]
    }>()
)

export const getDontWantIngredients = createAction(
    "Get Dont Want ingredients",
    props<{
        ingredients: Ingredient[]
    }>()
)

export const removeWantIngredient = createAction(
    "Remove Want Ingredient",
    props<{
        ingredient: Ingredient
    }>()
)

export const removeDontWantIngredient = createAction(
    "Remove Dont Want Ingredient",
    props<{
        ingredient: Ingredient
    }>()
)

export const putWantIngredient = createAction(
    "Put Want Ingredient",
    props<{
        ingredient: Ingredient
    }>()
)

export const putDontWantIngredient = createAction(
    "Put Dont Want Ingredient",
    props<{
        ingredient: Ingredient
    }>()
)





// export const getAllPizzas = createAction(
//     "Get All Pizzas",
//     props<{
//         pizzas: Pizza[]
//     }>()
// )

// export const getPizzas = createAction(
//     "Get Pizzas With Specific Ingredients",
//     props<{
//         wantedIngredientsID: number[],
//         avoidIngredientsID: number[],
//         pizzas: Pizza[]
//     }>()
// )