import { state } from "@angular/animations";
import { createEntityAdapter, EntityState } from "@ngrx/entity";
import { createReducer, on, Store } from "@ngrx/store";
import { Ingredient } from "../models/ingredient";
import * as Actions from "./ingredients.actions";
import * as PizzaActions from "./pizzas.actions"


// export interface PizzaState extends EntityState<Pizza> {
//     //SortedPizzas: ReadonlyArray<Pizza>;  // Pice sa specificnim sastojcima
// }

// const adapter = createEntityAdapter<Pizza>()

// export const initialState: PizzaState = adapter.getInitialState();
export interface allIngredientsState extends EntityState<Ingredient>{
}

export interface wantIngredientsState extends EntityState<Ingredient>{
}

export interface dontWantIngredientsState extends EntityState<Ingredient>{
}

const adapter = createEntityAdapter<Ingredient>();

export const AllinitialState: wantIngredientsState = adapter.getInitialState();

export const WantinitialState: wantIngredientsState = adapter.getInitialState();

export const DontWantinitialState: wantIngredientsState = adapter.getInitialState();

export const AllIngredientsReducer = createReducer(
    AllinitialState,
    on(Actions.loadAllIngredientsSucces, (state, {ingredients}) => adapter.setAll(ingredients, state))
);

export const WantIngredientsReducer = createReducer(
    WantinitialState,
    on(Actions.getWantIngredients, state => ({
        ...state
    })),
    on(Actions.removeWantIngredient, (state, { ingredient }) => adapter.removeOne(ingredient.id, state)
    ),
    on(Actions.putWantIngredient, (state, { ingredient }) => adapter.addOne(ingredient, state)
    )
);

export const DontWantIngredientsReducer = createReducer(
    DontWantinitialState,
    on(Actions.getDontWantIngredients, state => ({
        ...state
    })),
    on(Actions.removeDontWantIngredient, (state, { ingredient }) => adapter.removeOne(ingredient.id, state)
    ),
    on(Actions.putDontWantIngredient, (state, { ingredient }) => adapter.addOne(ingredient, state)
    )
);
