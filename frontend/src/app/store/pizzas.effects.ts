import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { of } from "rxjs";
import { catchError, map, mergeMap } from "rxjs/operators";
import { PizzasService } from "../services/pizzas.service";
import * as PizzasActions from "./pizzas.actions"

// @Injectable()
// export class PizzasEffect {
//     constructor(private pizzaService: PizzasService, private actions$: Actions) { }

//     loadEffects$ = createEffect(() => 
//         this.actions$.pipe(
//             ofType(PizzaActions.getPizzas),
//             mergeMap(() => this.pizzaService.getAll()
//                 .pipe(
//                     map((pizzas) => (PizzaActions.getAllPizzas({ pizzas }))),
//                     catchError(() => of({ type: "load error" }))
//                 )
//             )
//         )
//     )
// }


@Injectable()
export class PizzasEffect {
    constructor(private pizzaService: PizzasService, private actions$: Actions) { }

    loadEffect$ = createEffect(() =>
        this.actions$.pipe(
            ofType(PizzasActions.loadPizzas),
            mergeMap(() => this.pizzaService.getAll()
                .pipe(
                    map((pizzas) => (PizzasActions.loadPizzasSuccess({ pizzas }))),
                    catchError(() => of({ type: "load error" }))
                )
            )
        )
    )

    loadSpecificEffect$ = createEffect(() =>
        this.actions$.pipe(
            ofType(PizzasActions.getSpecificPizzas),
            mergeMap((action) => this.pizzaService.getSpecific(action.wantedIngredientsID, action.avoidIngredientsID, action.daLiJeMeso, action.daLiJePosna)
                .pipe(
                    map((pizzas) => (PizzasActions.loadPizzasSuccess({ pizzas }))),
                    catchError(() => of({ type: "load error" }))
                )
            )
        )
    )

    // loadSpecificPOSTEffect$ = createEffect(() =>
    //     this.actions$.pipe(
    //         ofType(PizzasActions.getSpecificPizzasPOST),
    //         mergeMap((action) => this.pizzaService.getSpecificPOST(action.wantedIngredientsID, action.avoidIngredientsID)
    //             .pipe(
    //                 map((pizzas) => (PizzasActions.loadPizzasSuccess({ pizzas }))),
    //                 catchError(() => of({ type: "load error" }))
    //             )
    //         )
    //     )
    // )
}


