import { allIngredientsState, dontWantIngredientsState, wantIngredientsState } from "../store/ingredients.reducer";
import { PizzaState } from "../store/pizzas.reducer";


export interface AppState {
    pizzas: PizzaState;
    AllIngredients: allIngredientsState;
    WantIngredients: wantIngredientsState;
    DontWantIngredients: dontWantIngredientsState;
}
