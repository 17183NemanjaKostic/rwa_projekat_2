import { createAction, props } from "@ngrx/store";
import { Pizza } from "../models/pizza";


export const getPizzas = createAction(
    "Get All Pizzas",
    props<{
        pizzas: Pizza[]
    }>()
)

export const getSpecificPizzas = createAction(
    "Get Pizzas With Specific Ingredients",
    props<{
        wantedIngredientsID: number[],
        avoidIngredientsID: number[],
        daLiJeMeso: boolean,
        daLiJePosna: boolean
    }>()
)

export const loadPizzasSuccess = createAction(
    "Load pizzas Success",
    props<{pizzas: Pizza[]}>()
);

export const loadPizzas = createAction(
    "Load pizzas"
);


