import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { of } from "rxjs";
import { catchError, map, mergeMap } from "rxjs/operators";
import { PizzasService } from "../services/pizzas.service";
import * as IngredientsActions from "./ingredients.actions"

@Injectable()
export class IngredientssEffect {
    constructor(private pizzaService: PizzasService, private actions$: Actions) { }

    loadEffect$ = createEffect(() =>
        this.actions$.pipe(
            ofType(IngredientsActions.loadAllIngredients),
            mergeMap(() => this.pizzaService.getAllIngredients()
                .pipe(
                    map((ingredients) => (IngredientsActions.loadAllIngredientsSucces({ ingredients }))),
                    catchError(() => of({ type: "load error" }))
                )
            )
        )
    )

}