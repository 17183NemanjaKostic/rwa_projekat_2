import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Pizza } from '../models/pizza';
import { environment } from '../../environments/environment'
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Ingredient } from '../models/ingredient';

@Injectable({
  providedIn: 'root'
})
export class PizzasService {

  constructor(private httpClient: HttpClient) { }

  getAll() {
    return this.httpClient
    .get<Pizza[]>(environment.apiUrl + "pizzas/getAll")
    .pipe(
    catchError(errorHandler))
  }

  getAllIngredients() {
    return this.httpClient
    .get<Ingredient[]>(environment.apiUrl + "pizzas/getAllIngredients")
    .pipe(
    catchError(errorHandler))
  }


  getSpecific(wantedIngredients: number[], dontWantedIngredients: number[], daLiJeMeso: boolean, daLiJePosna: boolean) {
    let httpParams = new HttpParams()
    let httpHeaders = new HttpHeaders()
    httpParams = httpParams.append('wantedIngredientsID', wantedIngredients.join(','))
    httpParams = httpParams.append('avoidIngredientsID', dontWantedIngredients.join(','))
    httpParams = httpParams.append('daLiJeMeso', daLiJeMeso)
    httpParams = httpParams.append('daLiJePosna', daLiJePosna);
    httpHeaders = httpHeaders.append('Access-Control-Allow-Origin', "http://localhost:9999")
    return this.httpClient  
    .get<Pizza[]>(environment.apiUrl + "pizzas/getSpecific", { params: httpParams, headers: httpHeaders })
    .pipe(
    catchError(errorHandler))
  }

  // getSpecific(wantedIngredients: number[], dontWantedIngredients: number[]) {
  //   let httpParams = new HttpParams();
  //   httpParams = httpParams.append('wantedIngredientsID', JSON.stringify(wantedIngredients));
  //   httpParams = httpParams.append('avoidIngredientsID', JSON.stringify(dontWantedIngredients));
  //   return this.httpClient  
  //   .get<Pizza[]>(environment.apiUrl + "pizzas/getSpecific", { params: httpParams })
  //   .pipe(
  //   catchError(errorHandler))
  // }
} 

// let actorList = ['Elvis', 'Jane', 'Frances']
// let params = new HttpParams();
// params = params.append('actors', actorList.join(', '));
// this.http.get(url, { params: params })

const errorHandler = (error: HttpErrorResponse) => {
const errorMessage =
  error.status === 0
    ? `Can't connect to API ${error.error}`
    : `Backend returned code ${error.status}`;

return throwError(errorMessage);
};