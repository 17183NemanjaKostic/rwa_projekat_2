import { Component, OnInit } from '@angular/core';
import { Pizza } from './models/pizza';
import { loadPizzas, getSpecificPizzas } from './store/pizzas.actions';
import { Store } from '@ngrx/store';
import { AppState } from './store/app.state';
import { loadAllIngredients } from './store/ingredients.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  constructor(private store: Store<AppState>) {
  }
  ngOnInit(): void {
    this.store.dispatch(loadAllIngredients());
    // Ovo izbaciti ako se ne zeli da se povucu sve pice na pocetku
    this.store.dispatch(getSpecificPizzas({wantedIngredientsID: [], avoidIngredientsID: [], daLiJeMeso: true, daLiJePosna: false}));
    
  }
  title = 'frontend';
}

