export interface Ingredient {
    id: number,
    imeSastojka: string,
    daLiJeMeso: boolean,
    daLiJePosno: boolean,
    kalorije: number
}