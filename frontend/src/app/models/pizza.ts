import { Ingredient } from "./ingredient";

export interface Pizza {
    id: number,
    name: string,
    price: number,
    image: string,
    ingredients: Ingredient[]
}