import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppNavbarComponent } from './components/app-navbar/app-navbar.component';
import { PizzaDetailsComponent } from './components/pizza-details/pizza-details.component';
import { PizzaListComponent } from './components/pizza-list/pizza-list.component';
import { PizzasService } from './services/pizzas.service';
import { StoreModule } from '@ngrx/store';
import { PizzaReducer } from './store/pizzas.reducer';
import { WantIngredientsReducer,  DontWantIngredientsReducer, AllIngredientsReducer} from './store/ingredients.reducer'
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { IngredientPillButtonComponent } from './components/ingredient-pill-button/ingredient-pill-button.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PizzasEffect } from './store/pizzas.effects';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IngredientSearchComponent } from './components/ingredient-search/ingredient-search.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import {MatSelectInfiniteScrollModule} from 'ng-mat-select-infinite-scroll';
import {MatInputModule} from '@angular/material/input';
import { IngredientssEffect } from './store/ingredients.effects';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    AppNavbarComponent,
    PizzaDetailsComponent,
    PizzaListComponent,
    IngredientPillButtonComponent,
    IngredientSearchComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot({pizzas: PizzaReducer, AllIngredients: AllIngredientsReducer, WantIngredients: WantIngredientsReducer, DontWantIngredients: DontWantIngredientsReducer}),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    EffectsModule.forRoot([PizzasEffect, IngredientssEffect]),
    FontAwesomeModule,
    NgbModule,
    ReactiveFormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatFormFieldModule,
    NgxMatSelectSearchModule,
    MatSlideToggleModule,
    MatSelectInfiniteScrollModule,
    MatInputModule,
    MatCheckboxModule,
    FormsModule
  ],
  providers: [PizzasService],
  bootstrap: [AppComponent]
})
export class AppModule { }
