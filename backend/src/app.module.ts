import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PizzasController } from './pizzas/pizzas.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { Pizza } from './models/pizza.entity';
import { PizzasService } from './pizzas/pizzas.service';
import { Ingredient } from './models/ingredient.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      database: 'pizzasdb',
      username: 'root',
      password: 'root',
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: true,
      // migrationsTableName : "migrations",
      // migrations: ["migration/*.js"],
      // cli: {
      //   "migrationsDir": "migration"
      // }
    }),
    TypeOrmModule.forFeature([Pizza, Ingredient]),
  ],
  controllers: [AppController, PizzasController],
  providers: [AppService, PizzasService],
})
export class AppModule {}

