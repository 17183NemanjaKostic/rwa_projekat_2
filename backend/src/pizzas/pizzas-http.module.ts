
import { Module } from '@nestjs/common';
import { AppModule } from 'src/app.module';
import { PizzasController } from './pizzas.controller';
import { PizzasService } from './pizzas.service';


@Module({
  imports: [AppModule],
  providers: [PizzasService],
  controllers: [PizzasController]
})
export class PizzaHttpModule {}
