import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import e from 'express';
import { Ingredient } from 'src/models/ingredient.entity';
import { Repository } from 'typeorm';
import { Pizza } from '../models/pizza.entity';

@Injectable()
export class PizzasService {
 // private readonly pizzas: Pizza[] = [];

  constructor(
    @InjectRepository(Pizza)
    private pizzasRepository: Repository<Pizza>,
    @InjectRepository(Ingredient)
    private ingredientsRepository: Repository<Ingredient>,
  ) {}

  // create(pizza: Pizza) {
  //   this.pizzas.push(pizza);
  // }

  // findAll(): Pizza[] {
  //   return this.pizzas;
  // }

  async readAll(): Promise<Pizza[]> {
    return await this.pizzasRepository.find({ relations: ["ingredients"]});
  }

  async readAllIngredients(): Promise<Ingredient[]> {
    return await this.ingredientsRepository.find();
    
  }

  async readSpecific(wantedIngredients: number[], dontWantedIngredients: number[], daLiJeMeso: string, daLiJePosno: string): Promise<Pizza[]> {
    let pizzas = await this.pizzasRepository.find({ relations: ["ingredients"]});
    let finalPizzas = [];
    
    pizzas.forEach(pizza => {
      let founded: number = 0;
      pizza.ingredients.forEach(ingredient => {
        if(wantedIngredients.includes(ingredient.id)){
          founded++;
        }
      })
      if(founded === wantedIngredients.length)
        finalPizzas.push(pizza);
    })

    pizzas = finalPizzas;
    finalPizzas = [];

    pizzas.forEach(pizza => {
      let founded: boolean = false;
      pizza.ingredients.forEach(async ingredient => {
        if(ingredient != null)
          {
            if (dontWantedIngredients.includes(ingredient.id)) 
              founded = true;
          }
        else  founded = true;
      });
      if (founded == false)
        finalPizzas.push(pizza);
    })

    pizzas = finalPizzas;
    finalPizzas = [];

    pizzas.forEach(pizza => {
      let founded: boolean = false;
      pizza.ingredients.forEach(async ingredient => {
        if(ingredient != null)
          {
            //console.log("daLiJeMeso: " + daLiJeMeso + ", ingredient.daLiJeMeso: " + ingredient.daLiJeMeso );
            if(daLiJeMeso == "false")
            {
              //console.log("prva, " + daLiJeMeso);
              if(ingredient.daLiJeMeso == true) // Ako korisnik zeli da nema mesa
              {
                //console.log("druga" + ingredient.daLiJeMeso);
                founded = true;
              } 
              
            }
            if(daLiJePosno == "true" && ingredient.daLiJePosno == false) // Ako korisnik zeli da je sastojak odnosno pica posna
              founded = true;
          }
        else  founded = true;
      });
      if (founded == false)
          finalPizzas.push(pizza);
    })

    return finalPizzas;
  }

  
}




// Transakcije sa bazom
// async createMany(users: User[]) {
//     const queryRunner = this.connection.createQueryRunner();
  
//     await queryRunner.connect();
//     await queryRunner.startTransaction();
//     try {
//       await queryRunner.manager.save(users[0]);
//       await queryRunner.manager.save(users[1]);
  
//       await queryRunner.commitTransaction();
//     } catch (err) {
//       // since we have errors lets rollback the changes we made
//       await queryRunner.rollbackTransaction();
//     } finally {
//       // you need to release a queryRunner which was manually instantiated
//       await queryRunner.release();
//     }
//   }


//LAKSI NACIN CALLBACK-STYLE
// async createMany(users: User[]) {
//     await this.connection.transaction(async manager => {
//       await manager.save(users[0]);
//       await manager.save(users[1]);
//     });
//   }