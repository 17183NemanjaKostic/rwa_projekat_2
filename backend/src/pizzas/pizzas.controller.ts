import { Body, Controller, Get, Param, Query } from '@nestjs/common';
import { Ingredient } from 'src/models/ingredient.entity';
import { Pizza } from 'src/models/pizza.entity';
import { PizzasService } from './pizzas.service';


@Controller('pizzas')
export class PizzasController {
    
    constructor(private pizzasService: PizzasService) {}

    @Get()
    findAll(): string {
        return "sve pice";
    }
    
    @Get('getAll')
    read(): Promise<Pizza[]> {
    return this.pizzasService.readAll();
    }


    @Get('getSpecific')
    getSpecific(@Query('wantedIngredientsID') wantedIngredientsID, @Query('avoidIngredientsID') avoidIngredientsID, @Query('daLiJeMeso') daLiJeMeso, @Query('daLiJePosna') daLiJePosno): Promise<Pizza[]> {
        return this.pizzasService.readSpecific(wantedIngredientsID===""? [] : wantedIngredientsID.split`,`.map(x=>+x), avoidIngredientsID===""? [] : avoidIngredientsID.split`,`.map(x=>+x), daLiJeMeso, daLiJePosno);
    }

    @Get('getAllIngredients')
    getAllIngredients(): Promise<Ingredient[]> {
    return this.pizzasService.readAllIngredients();
    }
  

}

interface IngredientsTDO {
    wantedIngredientsID: number[];
    avoidIngredientsID: number[];
}
