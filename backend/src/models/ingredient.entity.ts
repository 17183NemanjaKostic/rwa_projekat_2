import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, ManyToMany } from 'typeorm';
import { Pizza } from './pizza.entity';

@Entity()
export class Ingredient {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  imeSastojka: string;

  @Column()
  daLiJeMeso: boolean;

  @Column()
  daLiJePosno: boolean;

  @Column()
  kalorije: number;

  // @ManyToMany(type => Pizza, pizza => pizza.id )
  // PizzasId: number[];
  // @ManyToMany(type => Pizza, pizza => pizza.id)
  // pizza: Pizza;
}