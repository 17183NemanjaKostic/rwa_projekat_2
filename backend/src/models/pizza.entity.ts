import { Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import { Ingredient } from './ingredient.entity';

@Entity()
export class Pizza {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string; 

  @Column({ type: "float" })
  price: number;

  @Column()
  image: string;

  @ManyToMany(() => Ingredient)
  @JoinTable()
  ingredients: Ingredient[];

  // @OneToMany(type => Ingredient, ingredient => ingredient.id )
  // ingredientsId: number[];
}